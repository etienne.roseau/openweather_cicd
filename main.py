import functions_framework
import os
from dotenv import load_dotenv

# To save as a CSV file
from google.cloud import storage
import requests
import datetime
import json


load_dotenv() 
token = os.getenv("TOKEN")

# Openweather API call
city = 'paris'
url = f'http://api.openweathermap.org/data/2.5/weather?q={city}&appid={token}'
now = datetime.datetime.now()
date_now = now.strftime("%Y %m %d %H:%M:%S")

@functions_framework.http
def main(request):

    response = requests.get(url)
    print('lancement de la requete')

    if response.status_code == 200:
        raw_json = json.loads(response.text)

        clean_json = {
        "lat": raw_json["coord"]["lat"],
        "lon": raw_json["coord"]["lon"],
        "meteo_description": raw_json["weather"][0]["description"],
        "temperature_celsius": round(raw_json["main"]["temp"] - 273.15),
        "ville": raw_json["name"],
        "last_update": date_now}

        header = ','.join([str(k) for k in clean_json.keys()])
        line = ','.join([str(v) for v in clean_json.values()])
        csv_result = f"{header}\n{line}"

        # test : write localy
        with open('cicd_result.csv', "w") as f:
            f.write(f"{header}\n")
            f.write(line)

        with open('cicd_result.csv', "r") as f:
            for l in f.readlines():
                print(l)

        # write inside bucket
        storage_client = storage.Client()
        bucket_name = "etienne-bucket"
        blob_name = "cicd_result.csv"

        bucket = storage_client.get_bucket(bucket_name)
        blob = bucket.blob(blob_name)
        with blob.open("w") as f:
            f.write(csv_result)
        
        print("Done : cicd_result.csv inside bucket")
        return "200"


    else:
        print('Error:', response.status_code)
        return "500"


# if __name__ == "__main__":
#     openweather_api("lol")
