1. project gitlab created
2. gitlab runner installed on ubuntu
2. runner activated
    - docker
    - python:3.9
4. gitlab-ci.yml created
5. create a repo AR named ar-cicd-etienne
5. push to artifact reg :
```
gcloud auth configure-docker europe-west1-docker.pkg.dev
docker build -t europe-west1-docker.pkg.dev/ct-sandbox-295909/ar-cicd-etienne/cicd-openweather .
docker push europe-west1-docker.pkg.dev/ct-sandbox-295909/ar-cicd-etienne/cicd-openweather:latest ```