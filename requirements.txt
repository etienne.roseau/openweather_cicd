functions_framework==3.0.0
requests==2.18.0
python-dotenv==0.15.0
google-cloud-storage==2.10.0
